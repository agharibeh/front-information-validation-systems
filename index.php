<!DOCTYPE html>
<!--[if lt IE 7 ]> <html lang="en" class="ie6 ielt8"> <![endif]-->
<!--[if IE 7 ]>    <html lang="en" class="ie7 ielt8"> <![endif]-->
<!--[if IE 8 ]>    <html lang="en" class="ie8"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--> <html lang="en"> <!--<![endif]-->
    <head>
        <meta charset="utf-8">
        <title> Front Information Validation System </title>
        <script src="//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js" type="text/javascript"></script>
        <script src="js/init.js" type="text/javascript"></script>
        <link rel="stylesheet" type="text/css" href="css/page-style.css" />
    </head>
    <body>
        <div class="container">
            <div id="message-box" class="message-box" ></div>
            <section id="content">
                <form action="" method="POST" >
                    <h1>Fro.no </h1>
                    <div>
                        <input type="text"  name="user_email" placeholder="Your Email"  id="user_Email" />
                    </div>
                    <div>
                        <input type="text"  name="user_url"  placeholder="Your website"  id="user_url" />
                    </div>
                    <div>
                        <input type="text" name="user_phone" placeholder="Your Phone Number"  id="user_phone" />
                    </div>
                    <div>
                        <input type="button" value="Validate" id="send-data" />
                    </div>
                </form><!-- form -->
            </section><!-- content -->
        </div><!-- container -->
    </body>
</html>