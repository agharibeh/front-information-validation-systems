<?php
/**
 *   Test Class for Front\Validation\BaseValidator class 
 * @author  Ahmad Gharibeh <AMG2255@gmail.com>
 *  @copyright (c) Front Information
 *  @link  http://fro.no/
 *  @package Test
 *  @createDate 2 Nov 2014
 * 
 */
use Front\Validation\Validator;
use Front\Validation\BaseValidater;
use Front\Validation\EmailValidator;
use Front\Validation\NorwegianMobileValidator;
use Front\Validation\URLValidator;

class BaseValidatorTest extends PHPUnit_Framework_TestCase {

    /**
     * simple Test for BaseValidar Class
     */
    public function testValidator() {
        $inputData = [
            'email' => "nobody@exmple.com",
            "url" => "domain.com",
            "Norwegian_mobile" => "41111111"
        ];
        $formValidator = new BaseValidater();
        $formValidator->addRule('email', new EmailValidator());
        $formValidator->addRule('url', new URLValidator());
        $formValidator->addRule('Norwegian_mobile', new NorwegianMobileValidator());
        $result = $formValidator->check($inputData);
        $this->assertEquals(TRUE, $result);
    }

}
