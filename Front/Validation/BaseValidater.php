<?php

/**
 *   Master  Validator  Class    
 * @author  Ahmad Gharibeh <AMG2255@gmail.com>
 *  @copyright (c) Front Information
 *  @link  http://fro.no/
 *  @package Front\Validation
 *  @createDate 2 Nov 2014
 * 
 */

namespace Front\Validation;

class BaseValidater implements Validator {

    /**
     *
     * @var Array Array for rule names and validator child class for  this rule  
     */
    protected $validators;

    /**
     *
     * @var Array Error Text Messages  for validation
     */
    protected $error_messages;

    /**
     *  Class Constructor 
     */
    public function __construct() {
        $this->validators = [];
        $this->error_messages = [];
    }

    /**
     *  master validate method request validation for Every object  depend on it class 
     *   and return final validation  result.
     * @param type array of rules  and  validator classes  for those rules  
     * @return boolean
     */
    public function check($validation_data) {
        foreach ($this->validators as $name => $validator) {
            if (!$validator->check($validation_data[$name])) {
                $this->error_messages[] = $validator->get_Error_message();
            }
        }
        if (!empty($this->error_messages)) {
            return false;
        }
        return true;
    }

    /**
     *  add Validation rule  and  which subcalss work for it  
     * @param Sting  $name
     * @param \Front\Validation\Validator $validator_rule
     */
    public function addRule($name, Validator $validator_rule) {
        $this->validators[$name] = $validator_rule;
    }

    /**
     * get Error Messages  for  All  validation object 
     * @return array
     */
    public function get_Error_message() {
        return $this->error_messages;
    }

}