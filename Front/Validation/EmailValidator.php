<?php
/**
 *   Email Validator  Class   
 * @author  Ahmad Gharibeh <AMG2255@gmail.com>
 *  @copyright (c) Front Information
 *  @link  http://fro.no/
 *  @package Front\Validation
 *  @createDate 2 Nov 2014
 * 
 */
namespace Front\Validation;

class EmailValidator extends BaseValidater {

    /**
     * check Text  as  valid Email format or not  
     * @param String $email_text
     * @return boolean
     */
    public function check($email_text) {
        if (!filter_var($email_text, FILTER_VALIDATE_EMAIL)) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * get Error Message String for Class Error  Message 
     * @return string
     */
    public function get_Error_message() {
        return "INVALIED_EMAIL_ERROR_MESSAGE";
    }

}
