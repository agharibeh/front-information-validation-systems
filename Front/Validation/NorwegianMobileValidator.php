<?php

/**
 *   NorwegianMobileValidator  Class   
 * @author  Ahmad Gharibeh <AMG2255@gmail.com>
 *  @copyright (c) Front Information
 *  @link  http://fro.no/
 *  @package Front\Validation
 *  @createDate 2 Nov 2014
 * 
 */

namespace Front\Validation;

class NorwegianMobileValidator extends BaseValidater {

    /**
     *  Validate Data  if  its Norwegian Mobile Number or Not 
     * @param  $mobile_number
     * @return boolean
     */
    public function check($mobile_number) {
        if (preg_match("/^(((4|9){1})\d{7})|(((59){1})\d{6})$/", $mobile_number) == 0) {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * get Error Message String for Class Error  Message 
     * @return string
     */
    public function get_Error_message() {
        return "INVILAD_NORWAY_MOBILE_ERROR_MESSAGE";
    }

}
