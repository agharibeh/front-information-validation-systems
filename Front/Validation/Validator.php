<?php
/**
 *   Validator Interface    
 * @author  Ahmad Gharibeh <AMG2255@gmail.com>
 *  @copyright (c) Front Information
 *  @link  http://fro.no/
 *  @package Front\Validation
 *  @createDate 2 Nov 2014
 * 
 */

namespace Front\Validation;


interface  Validator {
    public function check($validation_data);
    public function addRule($rule_name, Validator $validator_class);
    public function get_Error_message();
}