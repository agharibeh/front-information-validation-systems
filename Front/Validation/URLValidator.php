<?php
/**
 *   URL Validator  Class   
 * @author  Ahmad Gharibeh <AMG2255@gmail.com>
 *  @copyright (c) Front Information
 *  @link  http://fro.no/
 *  @package Front\Validation
 *  @createDate 2 Nov 2014
 * 
 */
namespace Front\Validation;

class URLValidator extends BaseValidater {

    /**
     *  check Text if  as URL Fromat 
     * @param String  $url_text
     * @return boolean
     */
    public function check($url_text) {

        $url_text = trim($url_text);
        $url_text = parse_url($url_text, PHP_URL_SCHEME) === null ? 'http://' . $url_text : $url_text;

        if (!filter_var($url_text, FILTER_VALIDATE_URL, FILTER_FLAG_SCHEME_REQUIRED | FILTER_FLAG_HOST_REQUIRED)) {
            return FALSE;
        } else {
            //check host format is valid  ex : www.domainName.com or domain.com
            return $this->url_dot_count($url_text);
        }
    }

    /**
     * count Dots in String  and it's must be more this ! time if there was "www" String
     * @param type $url_str
     * @return boolean
     */
    public function url_dot_count($url_str) {

        $host = parse_url($url_str, PHP_URL_HOST);
        $dotcount = substr_count($host, '.');
        if ($dotcount > 0) {
            if ($dotcount == 1 && strpos($host, 'www.') === 0) {
                return FALSE;
            }
        } else {
            return FALSE;
        }
        return TRUE;
    }

    /**
     * get Error Message String for Class Error  Message 
     * @return string
     * 
     */
    public function get_Error_message() {
        return "INVALIED_URL_ERROR_MESSAGE";
    }

}