<?php error_reporting(0);
/**
 *   Validate.php  
 *  Control  for Validation submited Data  and Return valdition Result  as  JOSN object .
 * @author  Ahmad Gharibeh <AMG2255@gmail.com>
 *  @copyright (c) Front Information
 *  @link  http://fro.no/
 *  @createDate  2 Nov 2014
 *  @example  path_of_Script /validate.php?email=nobady@dmainname.com&url=domainname.com&mobile=95001122
 */

require_once('./vendor/autoload.php');

use Front\Validation\BaseValidater,
    Front\Validation\EmailValidator,
    Front\Validation\URLValidator,
    Front\Validation\NorwegianMobileValidator;

/**
 * @var  Array  Array for All Text Message  in System  to make easy for  translate and edit 
 */
$message_txt_EN = array();
$message_txt_EN['INVALID_EMPTY_DATA_MESSAGE'] = "No any object to Valid it";
$message_txt_EN['INVALID_RULE_ERROR_MESSAGE'] = "No any of submit objects is URL ,E-mail or Mobile Number.";
$message_txt_EN['INVALIED_EMAIL_ERROR_MESSAGE'] = " E-mail is invalid format, please check it again. ";
$message_txt_EN['INVALIED_URL_ERROR_MESSAGE'] = "URL is invalid format, please check it again. ";
$message_txt_EN['INVILAD_NORWAY_MOBILE_ERROR_MESSAGE'] = "Mobile number is not Vaild mobile number in Norway";
$message_txt_EN['SUCCESS_VALIDATION_MESSAGE'] = "Congratulation all data you were submitted is valid format.";

// add validation Rule that you  need  for this Form 

$formValidator = new BaseValidater();
$formValidator->addRule('email', new EmailValidator());
$formValidator->addRule('url', new URLValidator());
$formValidator->addRule('mobile', new NorwegianMobileValidator());

// get  sumbmited Data  form Api (get) or  index page "POST"
if (isset($_POST) && !empty($_POST)) {
    $submited_data = $_POST;
} else {
    $submited_data = $_GET;
}

// do Validation and  print result as JSON 

if (!empty($submited_data) && $formValidator->check($submited_data)) {
    echo json_encode(array('result' => TRUE, "message" => array($message_txt_EN['SUCCESS_VALIDATION_MESSAGE'])));
} else {
    $error_message = $formValidator->get_Error_message();
    foreach ($error_message as $er_key => $err_txt) {

        $error_message[$er_key] = $message_txt_EN[$err_txt];
    }
    echo json_encode(array('result' => False, "message" => $error_message));
}