/**
 *   int.js
 *   send data from HTML form as ajax Post Request  , recivice Result  , and show it in HTML page   
 * @author  Ahmad Gharibeh <AMG2255@gmail.com>
 *  @copyright (c) Front Information
 *  @link  http://fro.no/
 *  @package Front
 *  @createDate  26-10-2014
 *  @example  path_of_Script /validate.php?email=nobady@dmainname.com&url=domainname.com&mobile=95001122
 */
(function($) {
    $(document).ready(function() {

        $("#send-data").on("click", function() {
            $('#message-box').fadeOut(100);
            /* check  if  any Filed  is Empty */
            var empty_fields = false;
            $.each(['#user_Email', '#user_url', '#user_phone'], function(index, value) {
                $(value).removeClass('error-data');
                if ($(value).val().length === 0) {
                    $(value).addClass('error-data');
                    empty_fields = true;
                }
            });
            // send data to server  to validate  it
            if (!empty_fields) {
                var user_email = $('#user_Email').val();
                var user_url = $('#user_url').val();
                var user_mobile = $('#user_phone').val();
                $.post("validate.php", {email: user_email, url: user_url, mobile: user_mobile}, function(data) {
                    //check  result   and show Success and Error Message 
                    $('#message-box').empty().removeClass('error')
                    if (data.result == false) {
                        $('#message-box').addClass('error');
                    } else {
                        $('#message-box').addClass('success');
                    }
                    $.each(data.message, function(message_index, message_vlaue) {
                        $('#message-box').append('<p>' + message_vlaue + '</p>');
                    });
                    $('#message-box').slideDown(1000);

                }, "json");
            } else {
                return false;
            }



        });
    });
})(jQuery)

